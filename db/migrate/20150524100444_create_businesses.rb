class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :name
      t.string :description
      t.string :email
      t.string :password
      t.string :telephone
      t.string :logo

      t.timestamps null: false
    end
  end
end
