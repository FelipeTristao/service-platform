class AddBusinessToService < ActiveRecord::Migration
  def change
    add_reference :services, :business, index: true
    add_foreign_key :services, :businesses
  end
end
