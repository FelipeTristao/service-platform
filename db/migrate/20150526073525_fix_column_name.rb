class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :quote_types, :type, :type_s
  end
end
