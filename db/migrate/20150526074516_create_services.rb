class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :description
      t.references :service_category, index: true
      t.string :availability
      t.references :quote_type, index: true
      t.float :base_price

      t.timestamps null: false
    end
    add_foreign_key :services, :service_categories
    add_foreign_key :services, :quote_types
  end
end
