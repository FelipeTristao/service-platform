json.array!(@businesses) do |business|
  json.extract! business, :id, :name, :description, :email, :password, :telephone, :logo
  json.url business_url(business, format: :json)
end
