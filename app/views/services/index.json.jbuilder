json.array!(@services) do |service|
  json.extract! service, :id, :name, :description, :service_category_id, :availability, :quote_type_id, :base_price
  json.url service_url(service, format: :json)
end
