class SessionController < ApplicationController

  def create
    business = Business.find_by(id: 1)
    log_in business
    redirect_to business
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

  def show
    flash[:success] = "The Login and Sign-up function are not implemented yet. You are now signed-up as a default business."

    redirect_to '/businesses/1/'
  end
end
