module BusinessesHelper

  def default_business
    business = Business.find_by(id: session[:business_id]) || Business.find_by(id: 1)
  end
end
