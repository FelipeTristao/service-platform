module SessionHelper

  def log_in(business)
    session[:business_id] = business.id
  end

  def current_business
    @current_business = Business.find_by(id: session[:business_id])
  end

  def logged_in?
    !current_business.nil?
  end

  def log_out
    session.delete(:business_id)
    @current_business = nil
  end
end
